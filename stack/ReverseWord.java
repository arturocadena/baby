// Source: https://www.quora.com/What-are-some-real-world-applications-of-a-stack-data-structure/answers/27681246?srid=TKkP

import java.util.*;

class ReverseWord{

  public String reverse(String word) {
    Stack<String> stack = new Stack<String>();
    String[] letters = word.split("");
    String reverse = "";

    for(String letter : letters) {
      stack.push(letter);
    }

    while(!stack.empty()) {
      reverse += stack.pop();
    }

    return reverse;
  }

  public static void main(String args[]) {
    ReverseWord rw = new ReverseWord();
    String word = "school";
    System.out.println(rw.reverse(word));
    // A joke word meaning "fear of palindromes"
    String anotherWord = "aibohphobia";
    System.out.println(rw.reverse(anotherWord));
  }
}
